package gitlabci

import (
	"context"
	"fmt"

	"github.com/hashicorp/errwrap"
	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/logical"
)

func pathToken(b *backend) *framework.Path {
	return &framework.Path{
		Pattern: "tokens/" + framework.GenericNameWithAtRegex("name"),

		Fields: map[string]*framework.FieldSchema{
			"name": {
				Type:        framework.TypeString,
				Description: "Name of the role to use when generating token",
				DisplayAttrs: &framework.DisplayAttributes{
					Name: "Role Name",
				},
			},
		},

		Operations: map[logical.Operation]framework.OperationHandler{
			logical.ReadOperation: &framework.PathOperation{
				Callback: b.pathTokensRead,
				Summary:  "TODO",
			},
			logical.UpdateOperation: &framework.PathOperation{
				Callback: b.pathTokensRead,
				Summary:  "TODO",
			},
		},

		HelpSynopsis:    pathTokensHelpSyn,
		HelpDescription: pathTokensHelpDesc,
	}
}

func (b *backend) pathTokensRead(ctx context.Context, req *logical.Request, d *framework.FieldData) (*logical.Response, error) {
	roleName := d.Get("name").(string)

	role, err := b.roleRead(ctx, req.Storage, roleName, true)
	if err != nil {
		return nil, errwrap.Wrapf("error retrieving role: {{err}}", err)
	}
	if role == nil {
		return logical.ErrorResponse(fmt.Sprintf("Role '%s' not found", roleName)), nil
	}

	return b.tokenCreate(ctx, req.Storage, req.DisplayName, roleName, role)
}

const pathTokensHelpSyn = `Generate a new runner authentication token`
const pathTokensHelpDesc = `
This path is used to generate a new runner authentication token. A read
or write to this path will result in a new authentication token being
generated for the requested role.
`
