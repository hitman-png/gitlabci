# GitLab CI Token Secrets Engine

The GitLab CI token secrets engine generates GitLab runner authentication
tokens. This allows management of the lifecycle of an authentication
token to be delegated to the Vault installation.

## Quick Setup

1. The plugin directory must be set in the Vault configuration file.

   ```
   plugin_directory = "/etc/vault/vault_plugins"
   ```

   Place the `gitlabci` executable in this plugin directory. Ensure
   that proper permissions are in place to protect the plugin directory,
   and allow execuation of the `gitlabci` binary.

1. Register the gitlabci plugin with the Vault plugin catalog

   ```sh
   $ sha256sum /etc/vault/vault_plugins/gitlabci
   4727c400529e734e7cd403b70f15a60b3ab49f5723ee34023cd2e94bb50627c2  /etc/vault/vault_plugins/gitlabci

   $ vault write sys/plugins/catalog/secret/gitlabci \
       sha_256=4727c400529e734e7cd403b70f15a60b3ab49f5723ee34023cd2e94bb50627c2 \
       command=gitlabci
   Success! Data written to: sys/plugins/catalog/secret/gitlabci
   ```

1. Enable the GitLab CI token secret engine:

   ```sh
   $ vault secrets enable gitlabci
   Success! Enabled the gitlabci secrets engine at: gitlabci/
   ```

   By default, the secret engine will mount at `gitlabci`. Pass the
   `-path` argument to mount at a different path.

1. Create a role representing a CI runner configuration:

   ```sh
   $ vault write gitlabci/roles/example \
       gitlab_url=$GITLAB_URL \
       registration_token=$GITLAB_TOKEN \
       tags=$RUNNER_TAG
   Success! Data written to: gitlabci/roles/example
   ```

1. Request a runner authentication token

   ```sh
   $ vault read gitlabci/tokens/example
   ```

## Development Environment

Enter the development environment by running docker-compose:

```sh
docker-compose run --rm vault /bin/sh
```

Inside the development environment, the backend can be built and launched
within a vault configured for devmode by running:

```sh
make
```

While vault is running, in a separate terminal, the backend can be tested
with the following commands:

```sh
make enable
vault write gitlabci/roles/demo \
    gitlab_url=<gitlab_url> \
    registration_token=<runner_reg_token> \
    default_ttl=<auth_token_ttl> \
    tags=<tag_1> \
    tags=<tag_2>
vault read gitlabci/tokens/demo
```
