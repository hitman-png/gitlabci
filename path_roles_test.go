package gitlabci

import (
	"context"
	"testing"

	"github.com/hashicorp/vault/sdk/logical"
	"github.com/stretchr/testify/assert"
)

func TestBackend_PathRoleCreate_AccessToken(t *testing.T) {
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}

	b, err := Factory(context.Background(), config)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := b.HandleRequest(context.Background(), &logical.Request{
		Operation: logical.UpdateOperation,
		Path:      "roles/testrole",
		Storage:   config.StorageView,
		Data: map[string]interface{}{
			"gitlab_url":   "http://gitlab.example.com",
			"access_token": "test-access-token",
			"project_path": "/my/test/project",
		},
	})
	assert.Nil(t, err)
	assert.Nil(t, resp)
}

func TestBackend_PathRoleCreate_NoTag(t *testing.T) {
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}

	b, err := Factory(context.Background(), config)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := b.HandleRequest(context.Background(), &logical.Request{
		Operation: logical.UpdateOperation,
		Path:      "roles/testrole",
		Storage:   config.StorageView,
		Data: map[string]interface{}{
			"access_token": "test-access-token",
			"gitlab_url":   "http://gitlab.example.com",
			"project_path": "/my/test/project",
		},
	})
	assert.Nil(t, err)
	assert.Nil(t, resp)
}

func TestBackend_PathRoleUpdate_Empty(t *testing.T) {
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}

	b, err := Factory(context.Background(), config)
	if err != nil {
		t.Fatal(err)
	}

	resp, err := b.HandleRequest(context.Background(), &logical.Request{
		Operation: logical.UpdateOperation,
		Path:      "roles/testrole",
		Storage:   config.StorageView,
		Data: map[string]interface{}{
			"access_token": "test-access-token",
			"gitlab_url":   "http://gitlab.example.com",
			"project_path": "/my/test/project",
		},
	})
	assert.Nil(t, err)
	assert.Nil(t, resp)

	resp, err = b.HandleRequest(context.Background(), &logical.Request{
		Operation: logical.UpdateOperation,
		Path:      "roles/testrole",
		Storage:   config.StorageView,
		Data:      map[string]interface{}{},
	})
	assert.Nil(t, err)
	assert.Nil(t, resp)
}
