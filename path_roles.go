package gitlabci

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/hashicorp/go-multierror"
	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/logical"
)

func pathListRoles(b *backend) *framework.Path {
	return &framework.Path{
		Pattern: "roles/?$",

		Operations: map[logical.Operation]framework.OperationHandler{
			logical.ListOperation: &framework.PathOperation{
				Callback: b.pathRolesList,
				Summary:  "TODO",
			},
		},

		HelpSynopsis:    pathListRolesHelpSyn,
		HelpDescription: strings.TrimSpace(pathListRolesHelpDesc),
	}
}

func pathRoles(b *backend) *framework.Path {
	return &framework.Path{
		Pattern: "roles/" + framework.GenericNameWithAtRegex("name"),

		Fields: map[string]*framework.FieldSchema{
			"name": {
				Type:        framework.TypeString,
				Description: "Name of the role",
				DisplayAttrs: &framework.DisplayAttributes{
					Name: "Role Name",
				},
			},
			"access_token": {
				Type:        framework.TypeString,
				Description: "Access token used to lookup and register GitLab runners",
				DisplayAttrs: &framework.DisplayAttributes{
					Name: "Access Token",
				},
			},
			"default_ttl": {
				Type:        framework.TypeDurationSecond,
				Description: "Default ttl for role",
			},
			"gitlab_url": {
				Type:        framework.TypeString,
				Description: "URL used to access the GitLab instance",
				DisplayAttrs: &framework.DisplayAttributes{
					Name: "GitLab URL",
				},
			},
			"locked": {
				Type:        framework.TypeBool,
				Description: "True if runner's project set is locked",
			},
			"max_ttl": {
				Type:        framework.TypeDurationSecond,
				Description: "Maximum time a token is valid for",
			},
			"project_path": {
				Type:        framework.TypeStringSlice,
				Description: "Paths to project for runner registration",
				DisplayAttrs: &framework.DisplayAttributes{
					Name: "Project Path",
				},
			},
			"tags": {
				Type:        framework.TypeStringSlice,
				Description: "List of tags associated with a register runner",
			},
		},

		Operations: map[logical.Operation]framework.OperationHandler{
			logical.DeleteOperation: &framework.PathOperation{
				Callback: b.pathRoleDelete,
				Summary:  "TODO",
			},
			logical.ReadOperation: &framework.PathOperation{
				Callback: b.pathRoleRead,
				Summary:  "TODO",
			},
			logical.UpdateOperation: &framework.PathOperation{
				Callback: b.pathRoleWrite,
				Summary:  "TODO",
			},
		},

		HelpSynopsis:    pathRolesHelpSyn,
		HelpDescription: pathRolesHelpDesc,
	}
}

func (b *backend) pathRolesList(ctx context.Context, req *logical.Request, d *framework.FieldData) (*logical.Response, error) {
	b.roleMutex.RLock()
	defer b.roleMutex.RUnlock()
	entries, err := req.Storage.List(ctx, "role/")
	if err != nil {
		return nil, err
	}
	return logical.ListResponse(entries), nil
}

func (b *backend) pathRoleDelete(ctx context.Context, req *logical.Request, d *framework.FieldData) (*logical.Response, error) {
	err := req.Storage.Delete(ctx, "role/"+d.Get("name").(string))
	return nil, err
}

func (b *backend) pathRoleRead(ctx context.Context, req *logical.Request, d *framework.FieldData) (*logical.Response, error) {
	entry, err := b.roleRead(ctx, req.Storage, d.Get("name").(string), true)
	if err != nil {
		return nil, err
	}
	if entry == nil {
		return nil, nil
	}
	return &logical.Response{
		Data: entry.toResponseData(),
	}, nil
}

func (b *backend) pathRoleWrite(ctx context.Context, req *logical.Request, d *framework.FieldData) (*logical.Response, error) {
	roleName := d.Get("name").(string)
	if roleName == "" {
		return logical.ErrorResponse("missing role name"), nil
	}

	b.roleMutex.Lock()
	defer b.roleMutex.Unlock()
	roleEntry, err := b.roleRead(ctx, req.Storage, roleName, false)
	if err != nil {
		return nil, err
	}
	createOperation := false
	if roleEntry == nil {
		roleEntry = &gitlabciRoleEntry{}
		createOperation = true
	}

	if accessTokenRaw, ok := d.GetOk("access_token"); ok {
		roleEntry.AccessToken = accessTokenRaw.(string)
	} else if createOperation {
		roleEntry.AccessToken = d.Get("access_token").(string)
	}

	if defaultTTLRaw, ok := d.GetOk("default_ttl"); ok {
		roleEntry.DefaultTTL = time.Duration(defaultTTLRaw.(int)) * time.Second
	} else if createOperation {
		roleEntry.DefaultTTL = time.Duration(d.Get("default_ttl").(int)) * time.Second
	}

	if gitlabUrlRaw, ok := d.GetOk("gitlab_url"); ok {
		roleEntry.GitlabUrl = gitlabUrlRaw.(string)
	} else if createOperation {
		roleEntry.GitlabUrl = d.Get("gitlab_url").(string)
	}

	if maxTTLRaw, ok := d.GetOk("max_ttl"); ok {
		roleEntry.MaxTTL = time.Duration(maxTTLRaw.(int)) * time.Second
	} else if createOperation {
		roleEntry.MaxTTL = time.Duration(d.Get("max_ttl").(int)) * time.Second
	}

	if lockedRaw, ok := d.GetOk("locked"); ok {
		roleEntry.Locked = lockedRaw.(bool)
	} else if createOperation {
		roleEntry.Locked = d.Get("locked").(bool)
	}

	if projectPathRaw, ok := d.GetOk("project_path"); ok {
		roleEntry.ProjectPath = projectPathRaw.([]string)
	} else if createOperation {
		roleEntry.ProjectPath = d.Get("project_path").([]string)
	}

	if tagsRaw, ok := d.GetOk("tags"); ok {
		roleEntry.Tags = tagsRaw.([]string)
	} else if createOperation {
		roleEntry.Tags = d.Get("tags").([]string)
	}

	err = roleEntry.validate()
	if err != nil {
		return logical.ErrorResponse(fmt.Sprintf("error(s) validating supplied role data: %q", err)), nil
	}

	err = setPlayRole(ctx, req.Storage, roleName, roleEntry)
	if err != nil {
		return nil, err
	}

	return nil, nil
}

func (b *backend) roleRead(ctx context.Context, s logical.Storage, roleName string, shouldLock bool) (*gitlabciRoleEntry, error) {
	if roleName == "" {
		return nil, fmt.Errorf("missing role name")
	}
	if shouldLock {
		b.roleMutex.RLock()
	}
	entry, err := s.Get(ctx, "role/"+roleName)
	if shouldLock {
		b.roleMutex.RUnlock()
	}
	if err != nil {
		return nil, err
	}
	var roleEntry gitlabciRoleEntry
	if entry != nil {
		if err := entry.DecodeJSON(&roleEntry); err != nil {
			return nil, err
		}
		return &roleEntry, nil
	}
	return nil, nil
}

func setPlayRole(ctx context.Context, s logical.Storage, roleName string, roleEntry *gitlabciRoleEntry) error {
	if roleName == "" {
		return fmt.Errorf("empty role name")
	}
	if roleEntry == nil {
		return fmt.Errorf("nil roleEntry")
	}
	entry, err := logical.StorageEntryJSON("role/"+roleName, roleEntry)
	if err != nil {
		return err
	}
	if entry == nil {
		return fmt.Errorf("nil result when writing to storage")
	}
	if err := s.Put(ctx, entry); err != nil {
		return err
	}
	return nil
}

type gitlabciRoleEntry struct {
	AccessToken string        `json:"access_token"`
	DefaultTTL  time.Duration `json:"default_ttl"`
	GitlabUrl   string        `json:"gitlab_url"`
	MaxTTL      time.Duration `json:"max_ttl"`
	ProjectPath []string      `json:"project_path"`
	Tags        []string      `json:"tags"`
	Locked      bool          `json:"locked"`
}

func (r *gitlabciRoleEntry) toResponseData() map[string]interface{} {
	return map[string]interface{}{
		"default_ttl":   r.DefaultTTL.Seconds(),
		"gitlab_url":    r.GitlabUrl,
		"max_ttl":       r.MaxTTL.Seconds(),
		"project_paths": r.ProjectPath,
		"tags":          r.Tags,
		"locked":        r.Locked,
	}
}

func (r *gitlabciRoleEntry) validate() error {
	var errors *multierror.Error

	if r.GitlabUrl == "" {
		errors = multierror.Append(errors, fmt.Errorf("The specified value for gitlab_url is invalid."))
	}

	if r.AccessToken == "" {
		errors = multierror.Append(errors, fmt.Errorf("The specified value for access_token is invalid."))
	}

	if len(r.ProjectPath) == 0 {
		errors = multierror.Append(errors, fmt.Errorf("At least one project_path must be specified."))
	}

	return errors.ErrorOrNil()
}

const pathListRolesHelpSyn = `List the existing roles in this backend`
const pathListRolesHelpDesc = `Roles will be listed by the role name.`
const pathRolesHelpSyn = `Write, Read, and Delete GitLab CI roles`
const pathRolesHelpDesc = `
This path is used to define the registration parameters when creating
a runner token.

Changes to the gitlab_url and ttl parameters will affect the renewal
and revocation of existing authentication tokens. Other parameters only
affect new authentication tokens.

Delete operations will not revoke existing runner tokens. If the role
is deleted, attempting to renew or revoke existing tokens will result
in an error.
`
