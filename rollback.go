package gitlabci

import (
	"context"
	"fmt"

	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/helper/consts"
	"github.com/hashicorp/vault/sdk/logical"
	"github.com/mitchellh/mapstructure"
	"gitlab.com/slagit/vault/gitlabci/runner"
)

type walAuthnToken struct {
	AuthnToken string
	GitlabURL  string
}

func (b *backend) authnTokenWALRollback(ctx context.Context, req *logical.Request, kind string, data interface{}) error {
	var entry walAuthnToken
	if err := mapstructure.Decode(data, &entry); err != nil {
		return err
	}

	client := b.client()
	err := client.UnregisterRunner(
		ctx,
		&runner.UnregisterRunnerOptions{
			URL:                 entry.GitlabURL,
			AuthenticationToken: entry.AuthnToken,
		},
	)
	return err
}

type walDescription struct {
	AccessToken string
	Description string
	GitlabURL   string
}

func (b *backend) descriptionWALRollback(ctx context.Context, req *logical.Request, kind string, data interface{}) error {
	var entry walDescription
	if err := mapstructure.Decode(data, &entry); err != nil {
		return err
	}

	client := b.client()
	r, err := client.GetRunnerByDescription(ctx, &runner.GetRunnerByDescriptionOptions{
		AccessToken: entry.AccessToken,
		Description: entry.Description,
		URL:         entry.GitlabURL,
	})
	if err != nil {
		return err
	}

	err = client.RemoveRunner(ctx, &runner.RemoveRunnerOptions{
		AccessToken: entry.AccessToken,
		RunnerID:    r.ID,
		URL:         entry.GitlabURL,
	})
	return err
}

func (b *backend) walRollback(ctx context.Context, req *logical.Request, kind string, data interface{}) error {
	walRollbackMap := map[string]framework.WALRollbackFunc{
		"auth_token":  b.authnTokenWALRollback,
		"description": b.descriptionWALRollback,
	}

	if !b.System().LocalMount() && b.System().ReplicationState().HasState(consts.ReplicationPerformanceSecondary|consts.ReplicationPerformanceStandby) {
		return nil
	}

	f, ok := walRollbackMap[kind]
	if !ok {
		return fmt.Errorf("unknown kind to rollback")
	}

	return f(ctx, req, kind, data)
}
