package gitlabci

import (
	"context"
	"regexp"
	"testing"

	"github.com/hashicorp/vault/sdk/logical"
	"github.com/stretchr/testify/assert"
	"gitlab.com/slagit/vault/gitlabci/runner"
)

type mockRunnerClient struct {
	EnableCallback     func(context.Context, *runner.EnableRunnerOptions) error
	GetProjectCallback func(context.Context, *runner.GetProjectOptions) (*runner.Project, error)
	GetRunnerCallback  func(context.Context, *runner.GetRunnerByDescriptionOptions) (*runner.Runner, error)
	RegisterCallback   func(context.Context, *runner.RegisterRunnerOptions) (int64, string, error)
	RemoveCallback     func(context.Context, *runner.RemoveRunnerOptions) error
	UnregisterCallback func(context.Context, *runner.UnregisterRunnerOptions) error
}

func (m mockRunnerClient) EnableRunner(ctx context.Context, options *runner.EnableRunnerOptions) error {
	return m.EnableCallback(ctx, options)
}

func (m mockRunnerClient) GetProject(ctx context.Context, options *runner.GetProjectOptions) (*runner.Project, error) {
	return m.GetProjectCallback(ctx, options)
}

func (m mockRunnerClient) GetRunnerByDescription(ctx context.Context, options *runner.GetRunnerByDescriptionOptions) (*runner.Runner, error) {
	return m.GetRunnerCallback(ctx, options)
}

func (m mockRunnerClient) RegisterRunner(ctx context.Context, options *runner.RegisterRunnerOptions) (int64, string, error) {
	return m.RegisterCallback(ctx, options)
}

func (m mockRunnerClient) RemoveRunner(ctx context.Context, options *runner.RemoveRunnerOptions) error {
	return m.RemoveCallback(ctx, options)
}

func (m mockRunnerClient) UnregisterRunner(ctx context.Context, options *runner.UnregisterRunnerOptions) error {
	return m.UnregisterCallback(ctx, options)
}

func TestBackend_PathTokenAccess(t *testing.T) {
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}

	b := Backend()
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}

	resp, err := b.HandleRequest(context.Background(), &logical.Request{
		Operation: logical.UpdateOperation,
		Path:      "roles/testrole",
		Storage:   config.StorageView,
		Data: map[string]interface{}{
			"access_token": "test-access-token",
			"gitlab_url":   "http://gitlab.example.com",
			"project_path": "test/project/path",
			"tags":         []string{"tag-a"},
		},
	})
	assert.Nil(t, err)
	assert.Nil(t, resp)

	registerCalled := false
	b.runnerClient = mockRunnerClient{
		GetProjectCallback: func(ctx context.Context, options *runner.GetProjectOptions) (*runner.Project, error) {
			assert.EqualValues(t, "http://gitlab.example.com", options.URL)
			assert.EqualValues(t, "test-access-token", options.AccessToken)
			assert.EqualValues(t, "test/project/path", options.PathWithNamespace)
			return &runner.Project{
				ID:           1,
				RunnersToken: "test-reg-token",
			}, nil
		},
		RegisterCallback: func(ctx context.Context, options *runner.RegisterRunnerOptions) (int64, string, error) {
			assert.False(t, registerCalled)
			assert.EqualValues(t, "http://gitlab.example.com", options.URL)
			assert.EqualValues(t, "test-reg-token", options.RegistrationToken)
			assert.Regexp(t, regexp.MustCompile("^v-testrole-[a-zA-Z0-9]{20}-[0-9]+$"), options.Description)
			assert.EqualValues(t, []string{"tag-a"}, options.Tags)
			registerCalled = true
			return 1000, "test-auth-token", nil
		},
	}

	resp, err = b.HandleRequest(context.Background(), &logical.Request{
		Operation: logical.ReadOperation,
		Path:      "tokens/testrole",
		Storage:   config.StorageView,
	})
	assert.Nil(t, err)
	assert.NotNil(t, resp)
	assert.EqualValues(t, "http://gitlab.example.com", resp.Data["gitlab_url"])
	assert.EqualValues(t, "test-auth-token", resp.Data["authentication_token"])
	assert.True(t, registerCalled)
}

func TestBackend_PathTokenMultiple(t *testing.T) {
	config := logical.TestBackendConfig()
	config.StorageView = &logical.InmemStorage{}

	b := Backend()
	if err := b.Setup(context.Background(), config); err != nil {
		t.Fatal(err)
	}

	resp, err := b.HandleRequest(context.Background(), &logical.Request{
		Operation: logical.UpdateOperation,
		Path:      "roles/testrole",
		Storage:   config.StorageView,
		Data: map[string]interface{}{
			"access_token": "test-access-token",
			"gitlab_url":   "http://gitlab.example.com",
			"project_path": []string{"test/project/path", "test/project/other"},
			"tags":         []string{"tag-a"},
		},
	})
	assert.Nil(t, err)
	assert.Nil(t, resp)

	enableCalled := false
	registerCalled := false
	b.runnerClient = mockRunnerClient{
		EnableCallback: func(ctx context.Context, options *runner.EnableRunnerOptions) error {
			assert.False(t, enableCalled)
			assert.EqualValues(t, "http://gitlab.example.com", options.URL)
			assert.EqualValues(t, "test-access-token", options.AccessToken)
			assert.EqualValues(t, "test/project/other", options.PathWithNamespace)
			assert.EqualValues(t, 1000, options.RunnerID)
			enableCalled = true
			return nil
		},
		GetProjectCallback: func(ctx context.Context, options *runner.GetProjectOptions) (*runner.Project, error) {
			assert.EqualValues(t, "http://gitlab.example.com", options.URL)
			assert.EqualValues(t, "test-access-token", options.AccessToken)
			assert.EqualValues(t, "test/project/path", options.PathWithNamespace)
			return &runner.Project{
				ID:           1,
				RunnersToken: "test-reg-token",
			}, nil
		},
		RegisterCallback: func(ctx context.Context, options *runner.RegisterRunnerOptions) (int64, string, error) {
			assert.False(t, registerCalled)
			assert.EqualValues(t, "http://gitlab.example.com", options.URL)
			assert.EqualValues(t, "test-reg-token", options.RegistrationToken)
			assert.Regexp(t, regexp.MustCompile("^v-testrole-[a-zA-Z0-9]{20}-[0-9]+$"), options.Description)
			assert.EqualValues(t, []string{"tag-a"}, options.Tags)
			registerCalled = true
			return 1000, "test-auth-token", nil
		},
	}

	resp, err = b.HandleRequest(context.Background(), &logical.Request{
		Operation: logical.ReadOperation,
		Path:      "tokens/testrole",
		Storage:   config.StorageView,
	})
	assert.Nil(t, err)
	assert.NotNil(t, resp)
	assert.EqualValues(t, "http://gitlab.example.com", resp.Data["gitlab_url"])
	assert.EqualValues(t, "test-auth-token", resp.Data["authentication_token"])
	assert.True(t, enableCalled)
	assert.True(t, registerCalled)
}
