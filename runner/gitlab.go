package runner

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

type Project struct {
	ID                int64  `json:"id"`
	PathWithNamespace string `json:"path_with_namespace"`
	RunnersToken      string `json:"runners_token"`
}

type GetProjectOptions struct {
	AccessToken       string
	PathWithNamespace string
	URL               string
}

func (rc Client) GetProject(ctx context.Context, options *GetProjectOptions) (*Project, error) {
	req, err := http.NewRequestWithContext(ctx, "GET", options.URL+"/api/v4/projects/"+url.PathEscape(options.PathWithNamespace), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", "Bearer "+options.AccessToken)
	resp, err := rc.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GitLab project get returned %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var result Project
	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil, err
	}
	return &result, nil
}

type enableRequest struct {
	RunnerID int64 `json:"runner_id"`
}

type EnableRunnerOptions struct {
	AccessToken       string
	PathWithNamespace string
	RunnerID          int64
	URL               string
}

func (rc Client) EnableRunner(ctx context.Context, options *EnableRunnerOptions) error {
	requestBody, err := json.Marshal(enableRequest{
		RunnerID: options.RunnerID,
	})
	if err != nil {
		return err
	}

	req, err := http.NewRequestWithContext(ctx, "POST", options.URL+"/api/v4/projects/"+url.PathEscape(options.PathWithNamespace)+"/runners", bytes.NewBuffer(requestBody))
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+options.AccessToken)
	req.Header.Add("Content-Type", "application/json")
	resp, err := rc.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return fmt.Errorf("GitLab runner enable returned %d", resp.StatusCode)
	}
	return nil
}

type Runner struct {
	Description string `json:"description"`
	ID          int64  `json:"id"`
}

func (rc Client) listRunners(ctx context.Context, url, accessToken string, page *int) ([]Runner, *int, error) {
	var pageQuery string

	if page != nil {
		pageQuery = fmt.Sprintf("?page=%d", *page)
	}

	req, err := http.NewRequestWithContext(ctx, "GET", url+"/api/v4/runners"+pageQuery, nil)
	if err != nil {
		return nil, nil, err
	}
	req.Header.Add("Authorization", "Bearer "+accessToken)
	resp, err := rc.Do(req)
	if err != nil {
		return nil, nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return nil, nil, fmt.Errorf("GitLab runner list returned %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, nil, err
	}

	var result []Runner
	err = json.Unmarshal(body, &result)
	if err != nil {
		return nil, nil, err
	}

	var nextPagePtr *int
	nextPageRaw := resp.Header.Get("X-Next-Page")
	if nextPageRaw != "" {
		nextPage, err := strconv.Atoi(nextPageRaw)
		if err != nil {
			return nil, nil, err
		}
		nextPagePtr = &nextPage
	}
	return result, nextPagePtr, nil
}

type GetRunnerByDescriptionOptions struct {
	AccessToken string
	Description string
	URL         string
}

func (rc Client) GetRunnerByDescription(ctx context.Context, options *GetRunnerByDescriptionOptions) (*Runner, error) {
	runners, nextPage, err := rc.listRunners(ctx, options.URL, options.AccessToken, nil)
	for {
		if err != nil {
			return nil, err
		}
		for _, runner := range runners {
			if runner.Description == options.Description {
				return &runner, nil
			}
		}
		if nextPage == nil {
			break
		}
		runners, nextPage, err = rc.listRunners(ctx, options.URL, options.AccessToken, nextPage)
	}
	return nil, errors.New("Runner not found")
}

type RemoveRunnerOptions struct {
	AccessToken string
	RunnerID    int64
	URL         string
}

func (rc Client) RemoveRunner(ctx context.Context, options *RemoveRunnerOptions) error {
	req, err := http.NewRequestWithContext(ctx, "DELETE", fmt.Sprintf("%s/api/v4/runners/%d", options.URL, options.RunnerID), nil)
	if err != nil {
		return err
	}
	req.Header.Add("Authorization", "Bearer "+options.AccessToken)
	resp, err := rc.Do(req)
	if err != nil {
		return err
	}
	resp.Body.Close()
	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("GitLab runner remove returned %d", resp.StatusCode)
	}
	return nil
}
