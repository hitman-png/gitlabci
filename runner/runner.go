package runner

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

type RestClient interface {
	Do(*http.Request) (*http.Response, error)
}

type Client struct {
	RestClient
}

type registerRequest struct {
	Token       string   `json:"token"`
	Description string   `json:"description"`
	Active      bool     `json:"active"`
	Locked      bool     `json:"locked"`
	RunUntagged bool     `json:"run_untagged"`
	TagList     []string `json:"tag_list"`
	AccessLevel string   `json:"access_level"` // TODO: enum?
}

type registerResponse struct {
	ID    int64  `json:"id"`
	Token string `json:"token"`
}

type RegisterRunnerOptions struct {
	Description       string
	RegistrationToken string
	Tags              []string
	Locked            bool
	URL               string
}

func (rc Client) RegisterRunner(ctx context.Context, options *RegisterRunnerOptions) (int64, string, error) {
	requestBody, err := json.Marshal(registerRequest{
		Token:       options.RegistrationToken,
		Description: options.Description,
		Active:      true,
		Locked:      options.Locked,
		RunUntagged: len(options.Tags) == 0,
		TagList:     options.Tags,
		AccessLevel: "ref_protected",
	})
	if err != nil {
		return 0, "", err
	}

	req, err := http.NewRequestWithContext(ctx, "POST", options.URL+"/api/v4/runners", bytes.NewBuffer(requestBody))
	if err != nil {
		return 0, "", err
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err := rc.Do(req)
	if err != nil {
		return 0, "", err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return 0, "", fmt.Errorf("GitLab runner registration returned %d", resp.StatusCode)
	}

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return 0, "", err
	}

	var result registerResponse
	err = json.Unmarshal(body, &result)
	if err != nil {
		return 0, "", err
	}

	return result.ID, result.Token, nil
}

type unregisterRequest struct {
	Token string `json:"token"`
}

type UnregisterRunnerOptions struct {
	AuthenticationToken string
	URL                 string
}

func (rc Client) UnregisterRunner(ctx context.Context, options *UnregisterRunnerOptions) error {
	requestBody, err := json.Marshal(unregisterRequest{
		Token: options.AuthenticationToken,
	})
	if err != nil {
		return err
	}

	req, err := http.NewRequestWithContext(ctx, "DELETE", options.URL+"/api/v4/runners", bytes.NewBuffer(requestBody))
	if err != nil {
		return err
	}
	req.Header.Add("Content-Type", "application/json")
	resp, err := rc.Do(req)
	if err != nil {
		return err
	}
	resp.Body.Close()
	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("GitLab runner unregistration returned %d", resp.StatusCode)
	}
	return nil
}
