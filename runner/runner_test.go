package runner

import (
	"bytes"
	"context"
	"errors"
	"io/ioutil"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
)

type mockHTTPClient struct {
	Callback func(*http.Request) (*http.Response, error)
}

func (c mockHTTPClient) Do(req *http.Request) (*http.Response, error) {
	return c.Callback(req)
}

func mockRunnerClient(cb func(*http.Request) (*http.Response, error)) Client {
	return Client{
		mockHTTPClient{
			Callback: cb,
		},
	}
}

func TestRegisterRunner(t *testing.T) {
	json := `{"id":1000,"token": "test-auth-token"}`
	r := ioutil.NopCloser(bytes.NewReader([]byte(json)))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "POST", req.Method)
		assert.EqualValues(t, "gitlab.example.com", req.URL.Host)
		assert.EqualValues(t, "/api/v4/runners", req.URL.Path)
		body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		assert.JSONEq(t, `{"access_level":"ref_protected","active":true,"description":"test-runner","locked":false,"run_untagged":false,"tag_list":["tag_a","tag_b"],"token":"test-reg-token"}`, string(body))
		return &http.Response{
			StatusCode: http.StatusCreated,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	id, token, err := client.RegisterRunner(
		ctx,
		&RegisterRunnerOptions{
			URL:               "http://gitlab.example.com",
			RegistrationToken: "test-reg-token",
			Description:       "test-runner",
			Tags:              []string{"tag_a", "tag_b"},
		},
	)
	assert.Nil(t, err)
	assert.EqualValues(t, "test-auth-token", token)
	assert.EqualValues(t, 1000, id)
}

func TestRegisterRunnerLocked(t *testing.T) {
	json := `{"id":1000,"token": "test-auth-token"}`
	r := ioutil.NopCloser(bytes.NewReader([]byte(json)))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "POST", req.Method)
		assert.EqualValues(t, "gitlab.example.com", req.URL.Host)
		assert.EqualValues(t, "/api/v4/runners", req.URL.Path)
		body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		assert.JSONEq(t, `{"access_level":"ref_protected","active":true,"description":"test-runner","locked":true,"run_untagged":false,"tag_list":["tag_a","tag_b"],"token":"test-reg-token"}`, string(body))
		return &http.Response{
			StatusCode: http.StatusCreated,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	id, token, err := client.RegisterRunner(
		ctx,
		&RegisterRunnerOptions{
			URL:               "http://gitlab.example.com",
			RegistrationToken: "test-reg-token",
			Description:       "test-runner",
			Tags:              []string{"tag_a", "tag_b"},
			Locked:            true,
		},
	)
	assert.Nil(t, err)
	assert.EqualValues(t, "test-auth-token", token)
	assert.EqualValues(t, 1000, id)
}

func TestRegisterRunnerNoTags(t *testing.T) {
	json := `{"id":1000,"token": "test-auth-token"}`
	r := ioutil.NopCloser(bytes.NewReader([]byte(json)))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "POST", req.Method)
		assert.EqualValues(t, "gitlab.example.com", req.URL.Host)
		assert.EqualValues(t, "/api/v4/runners", req.URL.Path)
		body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		assert.JSONEq(t, `{"access_level":"ref_protected","active":true,"description":"test-runner","locked":false,"run_untagged":true,"tag_list":[],"token":"test-reg-token"}`, string(body))
		return &http.Response{
			StatusCode: http.StatusCreated,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	id, token, err := client.RegisterRunner(
		ctx,
		&RegisterRunnerOptions{
			URL:               "http://gitlab.example.com",
			RegistrationToken: "test-reg-token",
			Description:       "test-runner",
			Tags:              []string{},
		},
	)
	assert.Nil(t, err)
	assert.EqualValues(t, "test-auth-token", token)
	assert.EqualValues(t, 1000, id)
}

func TestRegisterRunnerBadToken(t *testing.T) {
	r := ioutil.NopCloser(bytes.NewReader([]byte{}))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		return &http.Response{
			StatusCode: http.StatusBadRequest,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	_, _, err := client.RegisterRunner(
		ctx,
		&RegisterRunnerOptions{
			URL:               "http://gitlab.example.com",
			RegistrationToken: "test-reg-token",
			Description:       "test-runner",
			Tags:              []string{},
		},
	)
	assert.NotNil(t, err)
}

func TestRegisterRunnerBadRequest(t *testing.T) {
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		return nil, errors.New("Test error while making request")
	})
	ctx := context.Background()

	_, _, err := client.RegisterRunner(
		ctx,
		&RegisterRunnerOptions{
			URL:               "http://gitlab.example.com",
			RegistrationToken: "test-reg-token",
			Description:       "test-runner",
			Tags:              []string{},
		},
	)
	assert.NotNil(t, err)
}

func TestUnregisterRunner(t *testing.T) {
	r := ioutil.NopCloser(bytes.NewReader([]byte{}))
	client := mockRunnerClient(func(req *http.Request) (*http.Response, error) {
		assert.EqualValues(t, "DELETE", req.Method)
		assert.EqualValues(t, "gitlab.example.com", req.URL.Host)
		assert.EqualValues(t, "/api/v4/runners", req.URL.Path)
		body, err := ioutil.ReadAll(req.Body)
		assert.Nil(t, err)
		assert.JSONEq(t, `{"token":"test-auth-token"}`, string(body))
		return &http.Response{
			StatusCode: http.StatusNoContent,
			Body:       r,
		}, nil
	})
	ctx := context.Background()

	err := client.UnregisterRunner(
		ctx,
		&UnregisterRunnerOptions{
			URL:                 "http://gitlab.example.com",
			AuthenticationToken: "test-auth-token",
		},
	)
	assert.Nil(t, err)
}
