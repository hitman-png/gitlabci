package gitlabci

import (
	"context"
	"net/http"
	"strings"
	"sync"
	"time"

	"github.com/hashicorp/vault/sdk/framework"
	"github.com/hashicorp/vault/sdk/logical"

	"gitlab.com/slagit/vault/gitlabci/runner"
)

func Factory(ctx context.Context, conf *logical.BackendConfig) (logical.Backend, error) {
	b := Backend()
	if err := b.Setup(ctx, conf); err != nil {
		return nil, err
	}
	return b, nil
}

func Backend() *backend {
	var b backend
	b.Backend = &framework.Backend{
		BackendType: logical.TypeLogical,
		Help:        strings.TrimSpace(gitlabciHelp),
		Paths: []*framework.Path{
			pathListRoles(&b),
			pathRoles(&b),
			pathToken(&b),
		},
		Secrets: []*framework.Secret{
			tokens(&b),
		},
		WALRollback:       b.walRollback,
		WALRollbackMinAge: 10 * time.Second,
	}
	return &b
}

type runnerClient interface {
	EnableRunner(context.Context, *runner.EnableRunnerOptions) error
	GetProject(context.Context, *runner.GetProjectOptions) (*runner.Project, error)
	GetRunnerByDescription(context.Context, *runner.GetRunnerByDescriptionOptions) (*runner.Runner, error)
	RegisterRunner(context.Context, *runner.RegisterRunnerOptions) (int64, string, error)
	RemoveRunner(context.Context, *runner.RemoveRunnerOptions) error
	UnregisterRunner(context.Context, *runner.UnregisterRunnerOptions) error
}

type backend struct {
	*framework.Backend

	roleMutex   sync.RWMutex
	clientMutex sync.RWMutex

	runnerClient runnerClient
}

func (b *backend) client() runnerClient {
	b.clientMutex.RLock()
	if b.runnerClient != nil {
		b.clientMutex.RUnlock()
		return b.runnerClient
	}

	b.clientMutex.RUnlock()
	b.clientMutex.Lock()
	defer b.clientMutex.Unlock()

	b.runnerClient = &runner.Client{
		RestClient: &http.Client{
			Timeout: time.Second * 10,
		},
	}
	return b.runnerClient
}

const gitlabciHelp = `
This backend manages GitLab CI tokens. The gitlabci backend manages the
full lifecycle of an authentication token. When a new token is requested
from the backend, it registers with the GitLab CI instance and returns
an authentication token. The runner will be removed from the GitLab
instance when the token lease expires.
`
